var gameUtils = {
  getStatus(cell) {
    return cell.getAttribute('data-status');
  },
  setStatus(cell, statusToSet) {
    cell.className = statusToSet;
    cell.setAttribute('data-status', statusToSet);
  },
  toggleStatus(cell) {
    if (gameUtils.getStatus(cell) === 'dead') {
      gameUtils.setStatus(cell, 'alive');
    } else {
      gameUtils.setStatus(cell, 'alive');
    }
  },
  selectCell(x, y) {
    return document.getElementById(x + '-' + y);
  },
  getNeighbors(cell) {

    var neighbors = [];

    var pos = cell.id.split('-').map(function(s) {
      return parseInt(s);
    });

    var sc = gameUtils.selectCell;

    // Same row adjacent.
    neighbors.push(sc(pos[0] - 1, pos[1]));
    neighbors.push(sc(pos[0] + 1, pos[1]));

    // Row above.
    neighbors.push(sc(pos[0] - 1, pos[1] - 1));
    neighbors.push(sc(pos[0], pos[1] - 1));
    neighbors.push(sc(pos[0] + 1, pos[1] - 1));

    // Row below.
    neighbors.push(sc(pos[0] - 1, pos[1] + 1));
    neighbors.push(sc(pos[0], pos[1] + 1));
    neighbors.push(sc(pos[0] + 1, pos[1] + 1));

    return neighbors.filter(function(cell) {
      return cell !== null;
    });

  },
  countLiveNeighbors: function(cell) {

    var neighbors = gameUtils.getNeighbors(cell);

    var liveNeighbors = neighbors.filter(function(neighbor) {
      return gameUtils.getStatus(neighbor) === 'alive';
    });

    return liveNeighbors.length + 1;

  }
};


var dimensions = 30
var gameOfLife = {

  width: dimensions,
  height: dimensions,
  stepInterval: null,

  createAndShowBoard: function() {
    var goltable = document.createElement("tbody");
    var tablehtml = '';
    for (var h = 0; h < this.height; h++) {
      tablehtml += "<tr id='row+" + h + "'>";
      for (var w = 0; w < this.width; w++) {
        tablehtml += "<td data-status='dead' id='" + w + "-" + h + "'></td>";
      }
      tablehtml += "</tr>";
    }
    goltable.innerHTML = tablehtml;
    var board = document.getElementById('board');
    board.appendChild(goltable);
    this.setupBoardEvents();
  },

  forEachCell: function(iteratorFunc) {
    var cells = document.getElementsByTagName('td');
    [].slice.call(cells).forEach(function(cellElement) {
      var coords = cellElement.id.split('-');
      iteratorFunc(cellElement, parseInt(coords[0]), parseInt(coords[1]));
    });
  },

  setupBoardEvents: function() {

    var onCellClick = function(e) {
      gameUtils.toggleStatus(this);
    };

    this.forEachCell(function(cell) {
      cell.onclick = onCellClick;
    });
    document.getElementById('board-size').onclick = this.changeDimensions.bind(this)
    document.getElementById('play_btn').onclick = this.enableAutoPlay.bind(this);
    document.getElementById('step_btn').onclick = this.step.bind(this);
    document.getElementById('clear_btn').onclick = this.clearBoard.bind(this);
    document.getElementById('reset_btn').onclick = this.createRandomBoard.bind(this);

  },
  changeDimensions: function() {
    if (this.width === size) {
      return
    }
    this.width = size.value
    this.height = size.value
    var board = document.getElementById('board')
    board.removeChild(board.firstChild)
    this.createAndShowBoard()
  },
  changeInterval: function() {

  },

  clearBoard: function() {

    this.forEachCell(function(cell) {
      gameUtils.setStatus(cell, 'dead');
    });

    this.stop();

  },

  createRandomBoard: function() {

    this.forEachCell(function(cell) {

      if (Math.random() > .5) {
        gameUtils.setStatus(cell, 'alive');
      } else {
        gameUtils.setStatus(cell, 'dead');
      }

    });

  },

  step: function() {
    var extinct = true
    var toToggle = [];

    this.forEachCell(function(cell) {

      var liveNeighborsCount = gameUtils.countLiveNeighbors(cell);

      if (gameUtils.getStatus(cell) === 'alive') {
        if (liveNeighborsCount !== 2 && liveNeighborsCount !== 3) {
          toToggle.push(cell);
        }
      } else {
        if (liveNeighborsCount === 3) {
          toToggle.push(cell);
          extinct = false
        }
      }

    });

    toToggle.forEach(gameUtils.toggleStatus);
    if (extinct) this.stop()
  },

  enableAutoPlay: function() {
    var self = this;

    if (this.stepInterval !== null) {
      this.stop();
    } else {
      this.stepInterval = setInterval(function() {
        self.step();
      }, 200);
    }

  },

  stop: function() {
    clearInterval(this.stepInterval);
    this.stepInterval = null;

  }
};

gameOfLife.createAndShowBoard();
